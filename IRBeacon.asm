; IR Beacon
    
; Version 0.0
; December 2020
;    
; Copyright (C) 2020 Steven J Lilley
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
    processor 16F84A

    #include <xc.inc>
    
; CONFIG
    CONFIG  FOSC = HS               ; Oscillator Selection bits (HS oscillator)
    CONFIG  WDTE = ON               ; Watchdog Timer (WDT enabled)
    CONFIG  PWRTE = ON              ; Power-up Timer Enable bit (Power-up Timer is enabled)
    CONFIG  CP = OFF                ; Code Protection bit (Code protection disabled)

; Registers
    STATUS_REG          equ 0x03
    OPTREG              equ 0x81

    BURST_COUNT         equ 0x0c    ; burst irPulse counter
    PARITY              equ 0x0d    ; bit 0 is used as a parity flag
    BIT_COUNT           equ 0x0e    ; transmitted bit counter
    PULSE_COUNT         equ 0x0f    ; counter for IR carrier irPulse
    MINS_ALARM          equ 0x10    ; Minutes alarm value
    HOUR_ALARM          equ 0x11    ; Hours alarm value
    BYTE_COUNT          equ 0x12    ; number of data bytes to read/write from/to RTC
      
                                    ;                   SPI     I2C
    SER_DATA0           equ 0x29    ; Serial data 0     -       Control
    SER_DATA1           equ 0x2a    ; Serial data 1     Addr    Addr
    SER_DATA2           equ 0x2b    ; Serial data 2
    SER_DATA3           equ 0x2c    ; Serial data 3
    SER_DATA4           equ 0x2d    ; Serial data 4
    SER_DATA5           equ 0x2e    ; Serial data 5
    SER_DATA6           equ 0x2f    ; Serial data 6
    SER_DATA7           equ 0x30    ; Serial data 7
    SER_DATA8           equ 0x31    ; Serial data 8

; PORTA bits
    IR_LED              equ 0
    I2C_DATA            equ 2       ; I2C data
    I2C_CLOCK           equ 3       ; I2C clock
        
; PORTB bits
    RTC_INT             equ 0       ; RTC interrupt input
    RTC_ENABLE          equ 1       ; RTC enable output
;    RTC_WARNING         equ 2       ; RTC not running warning output
    SPI_CLOCK           equ 3       ; SPI clock output
    MISO                equ 4       ; SPI master data input
    MOSI                equ 5       ; SPI master data output

; INTCON register
    GLOBAL_INT_EN       equ 7       ; Global interrupt enable flag bit
    RB0_INT             equ 1
     
; STATUS register
    REGBANK             equ 5       ; Bank indicator
    WATCHDOG            equ 4       ; Watchdog timeout
    ZEROFLAG            equ 2       ; Zero flag
    DIGITCARRY          equ 1       ; Digit carry flag
    CARRYBIT            equ 0       ; Carry flag
        
; **** Macros ****
      
disableInterrupts   MACRO
    bcf     INTCON, GLOBAL_INT_EN
    btfsc   INTCON, GLOBAL_INT_EN
ENDM

    
PSECT edata
    dw      0x70    ; control
    dw      0x01    ; register
    dw      0x0f    ; decode mode
    dw      0x18    ; intensity
    dw      0x03    ; scan limit
    dw      0x01    ; configuration
    dw      0x70    ; control
    dw      0x24    ; register
    dw      0x40    ; segment control
            
PSECT code abs
ORG 0x000
 
por:        
    call initialize

standby:    
    sleep
    nop
    nop
    
interrupt:  
    disableInterrupts
    goto    interrupt
    call    oscillatorStartup
    btfss   STATUS_REG, WATCHDOG
    call    transmit
    btfsc   INTCON, RB0_INT
    call    rtcInterrupt
    goto    standby

transmit:   
    disableInterrupts           ; disable interrupts
    goto transmit           
    clrwdt                      ; clear watchdog, gives 2s play time
    movlw   0x69
    movwf   SER_DATA0
    call    sendIrByte
    retfie

    ; Handle RB0/INT (generated by INT0 on the RTC)
    ;
    ; Read RTC from registers 1 to 7
    ;
    ; This gives us minutes through to alarm 0 seconds. We must read 
    ; the alarm register to clear the interrupt.
    ;
rtcInterrupt:     
    disableInterrupts
    goto    rtcInterrupt
    clrwdt
    call    readRtcTime
    call    dispTime
    bcf     INTCON, RB0_INT
    retfie
    
readRtcTime:
    movlw   0x07
    movwf   BYTE_COUNT
    movlw   SER_DATA1
    movwf   FSR
    movlw   0x01
    movwf   INDF
    call    rtcRead
    return

    ; Show the time
    ;
    ; This is called by the rtcInterrupt routine after it has read the time 
    ; into registers SER_DATA2 onward. We are only interested in minutes 
    ; and hours. These need spliting into tens and units for the display driver.
    ;
    ;   Register        After rtcRead       Required for display
    ;   ---------       ---------------     ---------------------
    ;   SER_DATA0       [unknown]           control (0x70)
    ;   SER_DATA1       address (0x01)      address (0x20)
    ;   SER_DATA2       minutes             minutes units
    ;   SER_DATA3       hours               minutes tens
    ;   SER_DATA4       day                 hours units
    ;   SER_DATA5       date                hours tens
    ;   SER_DATA6       month               [don't care]
    ;   SER_DATA7       year                [don't care]
    ;   SER_DATA8       alarm0 seconds      [don't care]
    ;            
dispTime:   
    clrwdt
    swapf   SER_DATA3, w        ; move hours tens to SER_DATA5
    andlw   0x0f
    movwf   SER_DATA5
    movf    SER_DATA3, w        ; move hours minutes to SER_DATA4
    andlw   0x0f
    movwf   SER_DATA4
    swapf   SER_DATA2, w        ; move minutes tens to SER_DATA3
    andlw   0x0f
    movwf   SER_DATA3
    movf    SER_DATA2, w        ; remove tens from minutes
    andlw   0x0f
    movwf   SER_DATA2
    movlw   SER_DATA1           ; point FSR to SER_DATA1
    movwf   FSR
    movlw   0x20                ; set address
    movwf   INDF
    decf    FSR, f
    movlw   0x70                ; set control
    movwf   INDF
    movlw   0x04                ; set data count
    movwf   BYTE_COUNT
    call    displayWrite
    return        

    ; ************************************************************************
    ; ********                     IR routines                        ********
    ; ************************************************************************

sendIrByte:   
    call    irSendBurst         ; start bit
    call    sendIrBits          ; data
    btfsc   PARITY, 0           ; parity
    call    irBitWait
    btfss   PARITY, 0
    call    irSendBurst
    call    irBitWait           ; stop bit
    bcf     PORTA, IR_LED
    return

sendIrBits:   
    movlw   0x07                ; number of data bits
    movwf   BIT_COUNT
    clrf    PARITY
sendIrBitLoop:    
    btfsc   SER_DATA0, 6
    call    irSendBurst
    btfss   SER_DATA0, 6
    call    irBitWait
    rlf     SER_DATA0, f
    decf    BIT_COUNT, f
    btfss   STATUS_REG, ZEROFLAG
    goto    sendIrBitLoop
    return

irBitWait:    
    movlw   0xae
    movwf   BURST_COUNT
irBitWaitLoop:   
    decfsz  BURST_COUNT, f
    goto    irBitWaitLoop
    incf    PARITY, f
    return

irSendBurst:  
    movlw   0x1e                ; irPulse count for IR burst
    movwf   BURST_COUNT
irSendBurstLoop:  
    bsf     PORTA, IR_LED
    movlw   0x01
    call    irPulse
    bcf     PORTA, IR_LED
    nop
    nop
    nop
    nop
    nop
    decfsz  BURST_COUNT, f
    goto    irSendBurstLoop
    bcf     PORTA, IR_LED
    return

irPulse:      
    movwf   PULSE_COUNT
irPulseLoop:  
    decfsz  PULSE_COUNT, f
    goto    irPulseLoop
    return

    ; ************************************************************************
    ; ********                  Display routines                      ********
    ; ************************************************************************

    ; Write to the display driver
displayWrite:  
    call    i2cStart
    call    i2cWrite            ; control byte
    incf    FSR, f
    call    i2cWrite            ; address
dispWriteLoop:
    incf    FSR, f
    call    i2cWrite            ; data
    decfsz  BYTE_COUNT
    goto    dispWriteLoop
    call    i2cStop
    return
            
    ; ************************************************************************
    ; ********                    RTC routines                        ********
    ; ************************************************************************

    ; Write to RTC
rtcWrite:   
    bsf     PORTB, RTC_ENABLE   ; enable RTC
    call    spiWrite
    incf    FSR, f              ; point to data
    call    spiWrite
    bcf     PORTB, RTC_ENABLE   ; disable RTC
    return

    ; read from RTC
    ;
    ; A read operation is a write of the address followed by a read/s
    ; to get the value/s.
rtcRead:    
    bsf     PORTB, RTC_ENABLE   ; enable RTC
    call    spiWrite            ; write address that we want to read
rtcReadLoop:
    incf    FSR, f
    call    spiRead
    decfsz  BYTE_COUNT, f
    goto    rtcReadLoop
    bcf     PORTB, RTC_ENABLE   ; disable RTC
    return

    
    ; ************************************************************************
    ; ********                    SPI routines                        ********
    ; ************************************************************************
    
spiRead:    
    movlw   0x08                ; data bit length
    movwf   BIT_COUNT
spiReadLoop:   
    call    spiReadBit
    rlf     INDF, f
    decfsz  BIT_COUNT, f
    goto    spiReadLoop
    bcf     PORTB, MOSI
    rlf     INDF, f
    return

spiReadBit:   
    bsf     PORTB, SPI_CLOCK
    nop
    btfsc   PORTB, MISO
    bsf     INDF, 7
    btfss   PORTB, MISO
    bcf     INDF, 7
    bcf     PORTB, SPI_CLOCK
    return

spiWrite:   
    movlw   0x08                ; data bit length
    movwf   BIT_COUNT
spiWriteLoop:   
    call    spiWriteBit
    rlf     INDF, f
    decfsz  BIT_COUNT, f
    goto    spiWriteLoop
    rlf     INDF, f
    bcf     PORTB, MOSI
    return

    ; write bit 7 of INDF via SPI
spiWriteBit:  
    btfsc   INDF, 7
    bsf     PORTB, MOSI         ; set data high
    btfss   INDF, 7
    bcf     PORTB, MOSI         ; set data low
    nop
    bsf     PORTB, SPI_CLOCK    ; clock irPulse start
    nop
    bcf     PORTB, SPI_CLOCK    ; clock irPulse end
    return
    

    ; ************************************************************************
    ; ********                    I2C routines                        ********
    ; ************************************************************************

    ; I2C START bit
i2cStart:   
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_DATA     ; data  -> output
    bsf     TRISA, I2C_CLOCK    ; clock -> input
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_DATA     ; force data low
    nop
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_CLOCK    ; clock -> output
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_CLOCK    ; force clock low
    return
            
    ; Write a byte to I2C and wait for slave to acknowledge
i2cWrite:   
    movlw   0x08                ; data bit length
    movwf   BIT_COUNT
i2cWriteLoop:   
    btfsc   INDF, 7
    call    i2cDataHighClock
    btfss   INDF, 7
    call    i2cDataLowClock
    rlf     INDF, f
    decfsz  BIT_COUNT, f
    goto    i2cWriteLoop
    rlf     INDF, f
    call    i2cDataRelease
    call    i2cClockRelease
i2cAckLoop: 
    btfsc   PORTA, I2C_DATA     ; wait for ACK
    goto    i2cAckLoop
    call    i2cClockLow
    return

i2cDataHighClock:   
    call    i2cDataRelease
    call    i2cClockRelease
    nop
    call    i2cClockLow
    return
            
i2cDataLowClock:   
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_DATA
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_DATA
    call    i2cClockRelease
    nop
    call    i2cClockLow
    return
            
i2cClockRelease:  
    bsf     STATUS, REGBANK     ; use bank 1
    bsf     TRISA, I2C_CLOCK
    bcf     STATUS, REGBANK     ; use bank 0
    nop
    return
            
i2cClockLow:  
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_CLOCK
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_CLOCK
    return
            
i2cDataRelease: 
    bsf     STATUS, REGBANK     ; use bank 1
    bsf     TRISA, I2C_DATA
    bcf     STATUS, REGBANK     ; use bank 0
    return
            
i2cDataLow: 
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_DATA
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_DATA
    return

i2cStop:    
    bcf     PORTA, I2C_DATA
    bsf     STATUS, REGBANK     ; use bank 1
    bsf     TRISA, I2C_CLOCK
    bsf     TRISA, I2C_DATA
    bcf     STATUS, REGBANK     ; use bank 0
    return
    
    ; ************************************************************************
    ; ********                  EEPROM routines                       ********
    ; ************************************************************************

    ; Copy BYTE_COUNT bytes from EEPROM starting at address in W register 
    ; into SER_DATA0 register onward. Before returning the FSR is set to
    ; SER_DATA0.
    ;
copyFromEeprom:
    movwf   EEADR
    movlw   SER_DATA0
    movwf   FSR
copyLoop:
    bsf     STATUS, REGBANK
    bsf     EECON1, 0
    bcf     STATUS, REGBANK
    movf    EEDATA, w
    movwf   INDF
    incf    EEADR, f
    incf    FSR, f
    decfsz  BYTE_COUNT, f
    goto    copyLoop
    movlw   SER_DATA0
    movwf   FSR
    return
    
    ; ************************************************************************
    ; ********              Initialization routines                   ********
    ; ************************************************************************
    
    ; initialise registers
initialize:       
    disableInterrupts           ; disable interrupts
    goto    initialize          ; (make sure)
    clrwdt
    bsf     STATUS, REGBANK     ; use bank 1
    movlw   0x1e
    movwf   TRISA               ; Port A, RA0 is an output, others are input
    movlw   0xd1
    movwf   TRISB               ; Port B, SPI and RTC
    movlw   0x3f
    movwf   OPTREG              ; set OPTION register
    bcf     STATUS, REGBANK     ; use bank 0
    clrf    PORTA
    clrf    PORTB
    movwf   MINS_ALARM          ; disable alarms
    movwf   HOUR_ALARM
    call    initializeRtc
    call    initializeDisplay
    call    transmit
    movlw   0x10
    movwf   INTCON              ; set interrupt control register
    retfie

    ; initialise RTC
initializeRtc:    
    movlw   0x01
    movwf   BYTE_COUNT          ; number of bytes to read into counter
    movlw   SER_DATA1
    movwf   FSR                 ; set FSR to SER_DATA1
    movlw   0x0f
    movwf   INDF                ; set address to read
    call    rtcRead
    btfsc   SER_DATA2, 7        ; test bit 7 from the read register
    goto    skipRtcStartOsc
    movlw   SER_DATA2
    movwf   FSR
    movlw   0x07
    movwf   INDF                ; register value -> SER_DATA2
    decf    FSR, f
    movlw   0x8f
    movwf   INDF                ; control register address -> SER_DATA1
    call    rtcWrite            ; enable write
    decf    FSR, f
    call    rtcWrite            ; start oscillator and set interrupt control
    movlw   0x00
    movwf   INDF                ; register value -> SER_DATA2
    decf    FSR, f
    movlw   0x87                ; set address
    movwf   INDF                ; alarm0 register address -> SER_DATA1
    call    rtcWrite
skipRtcStartOsc:
    call    readRtcTime
    call    dispTime
    bcf     INTCON, RB0_INT
    return


    ; Initialise the display driver
    ;
    ; Note that we write values in reverse order so that FSR is pointing
    ; to correct address to start the operation.
initializeDisplay:   
    clrwdt
    movlw   0x06
    movwf   BYTE_COUNT
    movlw   0x00
    call    copyFromEeprom
    movlw   0x04
    movwf   BYTE_COUNT
    call    displayWrite
    ; set decimal point on for digit 2
    movlw   0x03
    movwf   BYTE_COUNT
    movlw   0x06
    call    copyFromEeprom
    movlw   0x01
    movwf   BYTE_COUNT
    call    displayWrite
    return
    
    ; Oscillator startup cycles when waking from sleep
    ;
oscillatorStartup: 
    movlw   0x04
    movwf   BIT_COUNT
oscillatorStartupLoop:  
    movlw   0xff
    call    irPulse
    decfsz  BIT_COUNT, F
    goto    oscillatorStartupLoop
    return


    
    end

   